import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/classic_ligth.dart';
import 'package:schultetablegame/GamePages/classic_orginal_reverse.dart';
import 'package:schultetablegame/GamePages/classic_ligth_reverse.dart';
import 'package:schultetablegame/GamePages/classic_orginal.dart';
import 'package:schultetablegame/GamePages/memory.dart';
import 'package:schultetablegame/GamePages/reaction.dart';
import 'package:schultetablegame/dataOperation.dart';

/*
https://gitlab.com/katsella
*/

void main() => runApp(MainPage());

class MainPage extends StatefulWidget {
  _MainPageState createState() => _MainPageState();
}

class DropDownControls {
  int currentIndex;
  List<int> dropDownItems;
}

class _MainPageState extends State<MainPage> with DropDownControls {
  List<int> dropDownItems = [3, 4, 5, 6, 7, 8];
  int currentIndex = 1;
  DataOperation dataOperation;

  _MainPageState() {
    dataOperation = new DataOperation();
    dataOperation.getIntData("currentIndex").then((value) {
      setState(() {
        currentIndex = value;
      });
    });
  }

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  setCurrentIndex(String newValue) {
    setState(() {
      currentIndex =
          dropDownItems.indexWhere((element) => element.toString() == newValue);
    });
    dataOperation.setIntData("currentIndex", currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Row(
            children: [Text("Oyunlar"), Spacer(), dropDownList],
          ),
        ),
        body: SafeArea(
          child: Center(
            child: GridWidget(this),
          ),
        ),
      ),
    );
  }

  Widget get dropDownList {
    return DropdownButton<String>(
      value: dropDownItems[currentIndex].toString(),
      icon: Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
          color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold),
      underline: Container(
        height: 2,
        color: Colors.black,
      ),
      onChanged: (String newValue) {
        setCurrentIndex(newValue);
      },
      items: dropDownItems.map<DropdownMenuItem<String>>((int value) {
        return DropdownMenuItem<String>(
          value: value.toString(),
          child: Text(
            value.toString(),
            style: TextStyle(color: Colors.black),
          ),
        );
      }).toList(),
    );
  }
}

// ignore: must_be_immutable
class GridWidget extends StatefulWidget {
  DropDownControls _dropDownControls;
  GridWidget(this._dropDownControls);
  GridViewState createState() => GridViewState(this._dropDownControls);
}

class GridViewState extends State {
  DropDownControls _dropDownControls;

  List<GamePage> gridItems = [
    ReactionGamePage(5),
    ClassicLigthGamePage(5),
    ClassicLigthReverseGamePage(5),
    ClassicOrginalGamePage(5),
    ClassicOrginalhReverseGamePage(5),
    MemoryGamePage(5),
  ];
  GridViewState(this._dropDownControls);

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  gridViewSelectedItem(BuildContext context, GamePage gameItem) {
    gameItem.gridSize =
        _dropDownControls.dropDownItems[_dropDownControls.currentIndex];
    Navigator.push(context, MaterialPageRoute(builder: (context) => gameItem));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 500),
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Expanded(
                child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 2,
                  children: gridItems
                      .map(
                        (data) => GestureDetector(
                          onTap: () {
                            gridViewSelectedItem(context, data);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: data.gamePageState.menuColor,
                            ),
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            child: Center(
                              child: Text(data.gamePageState.name,
                                  style: TextStyle(
                                      fontSize: 22, color: Colors.white),
                                  textAlign: TextAlign.center),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
