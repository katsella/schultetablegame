import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:schultetablegame/dataOperation.dart';

// ignore: must_be_immutable
class StatsBar extends StatefulWidget {
  _StatsBar statsBar;
  StatsBar(String gameName, String gameMode) {
    statsBar = _StatsBar(gameName, gameMode);
  }
  _StatsBar createState() => statsBar;
}

abstract class StatsBarControls {
  timerStart();
  timerStop();
}

class _StatsBar extends State<StatsBar> with StatsBarControls {
  String gameName, gameMode;
  String strCurrentTimer = "0.0",
      strBestScore = "0.0",
      strCurrentBestScore = "0.0";
  double currentTimer = 0.0, bestScore = 0.0, currentBestScore = 0.0;
  Color backColor = Colors.teal, textColor = Colors.white;
  Timer timer;
  String lastScore = "0";
  DataOperation dataOperation;
  final int timerRefreshTime = 16; // ms

  _StatsBar(String gameName, String gameMode) {
    this.gameName = gameName;
    this.gameMode = gameMode;
    dataOperation = new DataOperation();
    dataOperation.getDoubleData(gameName + "_" + gameMode).then((value) {
      setState(() {
        bestScore = value;
        strBestScore = bestScore.toString();
      });
    });
  }

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  timerStart() {
    setState(() {
      timer = Timer.periodic(
        Duration(milliseconds: timerRefreshTime),
        (Timer t) => _setTime(),
      );
      print("Bitis");
    });
  }

  timerStop() {
    currentTimer = (timer.tick).toDouble() * timerRefreshTime / 1000;
    if (bestScore == 0.0 || currentTimer < bestScore) {
      bestScore = currentTimer;
      dataOperation.setDoubleData(gameName + "_" + gameMode, bestScore);
      strBestScore = bestScore.toStringAsFixed(3);
    }

    if (currentBestScore == 0.0 || currentTimer < currentBestScore) {
      currentBestScore = currentTimer;
      strCurrentBestScore = currentBestScore.toStringAsFixed(3);
    }
    setState(() {
      strCurrentTimer = currentTimer.toStringAsFixed(3);
      timer?.cancel();
    });
  }

  _setTime() {
    currentTimer = (timer.tick).toDouble() * timerRefreshTime / 1000;
    setState(() {
      strCurrentTimer = currentTimer.toStringAsFixed(3);
    });
  }

  final Widget iconBest = SvgPicture.asset("assets/icons/iconBest.svg",
      width: 25, height: 25, color: Color(0xFFF2FA00));

  final Widget iconBestBronze = SvgPicture.asset("assets/icons/iconBest.svg",
      width: 25, height: 25, color: Color(0xffcd7f32));

  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        color: backColor,
      ),
      child: GestureDetector(
        onTap: () => {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  iconBest,
                  Text(
                    strBestScore,
                    style: TextStyle(
                        color: textColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  strCurrentTimer,
                  style: TextStyle(
                      color: textColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  iconBestBronze,
                  Text(
                    strCurrentBestScore,
                    style: TextStyle(
                        color: textColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
