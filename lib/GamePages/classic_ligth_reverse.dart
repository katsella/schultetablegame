import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';


// ignore: must_be_immutable
class ClassicLigthReverseGamePage extends GamePage
{
  int gridSize;
  ClassicLigthReverseGamePage(this.gridSize);
  GamePageState gamePageState =  ClassicLigthReversePageState(5);
  @override
  GamePageState createState()
  {
    return gamePageState =  ClassicLigthReversePageState(this.gridSize);
  }
}

class ClassicLigthReversePageState extends GamePageState 
{
  ClassicLigthReversePageState(int gridSize) : super("Klasik Işık Ters", "classic_ligth_reverse", Colors.orange, gridSize)
  {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state) {
    if(isMenuActive) return;
    setState(() {
      if (expactingNum != properties.num) return;
      properties.color = Colors.transparent;
      properties.numVisible = false;
      if (properties.num == 1) {
        endGame();
        return;
      }
      expactingNum--;
    });
  }

  @override
  startGame()
  {
    super.startGame();
    expactingNum = totalGridSize;
    showBoxes();
  }

  @override 
  resetGame(){
    super.resetGame();
    this.expactingNum = totalGridSize;
  }
}
