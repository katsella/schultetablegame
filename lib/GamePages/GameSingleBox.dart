import 'package:flutter/material.dart';

class GSBProperties {
  int num;
  Color color;
  bool numVisible = true;
  GSBProperties(this.num, this.color, this.numVisible);
}

abstract class GSBControls {
  selectedItem(GSBProperties gsbProperties, GameSingleBoxState state);
}

// ignore: must_be_immutable
class GameSingleBox extends StatefulWidget {
  GameSingleBoxState gamePageState;
  final GSBProperties _gsbProperties;
  final GSBControls _gsbControls;
  GameSingleBox(this._gsbProperties, this._gsbControls);
  @override
  GameSingleBoxState createState() {
    return gamePageState = GameSingleBoxState(_gsbProperties, _gsbControls);
  }
}

class GameSingleBoxState extends State<GameSingleBox> {
  GSBProperties gsbProperties;
  GSBControls gsbControls;
  GameSingleBoxState(this.gsbProperties, this.gsbControls);

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  selectedBox() {
    setState(() {
      gsbControls.selectedItem(gsbProperties, this);
    });
  }

  showNumTemporary() { // şimdilik sadece hafıza oyununda kullanılıyor
     setState(() {
          gsbProperties.numVisible = true;
        });
    Future.delayed(
      Duration(seconds: 1),
      () {
        setState(() {
          gsbProperties.numVisible = false;
        });
      },
    );
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragStart: (a) => {selectedBox()},
      onTapDown: (a) => {selectedBox()},
      child: AnimatedContainer(
        curve: Curves.decelerate,
        duration: Duration(milliseconds: 150),
        decoration: BoxDecoration(
          color: gsbProperties.color,
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
        ),
        child: Center(
          child: Visibility(
            visible: gsbProperties.numVisible,
            child: Text(gsbProperties.num.toString(),
                style: TextStyle(fontSize: 35, color: Colors.black),
                textAlign: TextAlign.center),
          ),
        ),
      ),
    );
  }
}
