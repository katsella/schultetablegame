import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';


// ignore: must_be_immutable
class ReactionGamePage extends GamePage
{
  int gridSize;
  ReactionGamePage(this.gridSize);
  GamePageState gamePageState =  ReactionGamePageState(5);
  @override
  GamePageState createState()
  {
    return gamePageState =  ReactionGamePageState(this.gridSize);
  }
}

class ReactionGamePageState extends GamePageState 
{
  ReactionGamePageState(int gridSize) : super("Reaksiyon", "reaction", Colors.blue, gridSize)
  {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
  }

  highligthNextBox()
  {
    GameSingleBox gameSingleBox;
    gridItems.forEach((item) => {if(item.gamePageState.gsbProperties.num==expactingNum)gameSingleBox=item});
    gameSingleBox.gamePageState.gsbProperties.numVisible = true;
    gameSingleBox.gamePageState.gsbProperties.color = Colors.red;
    gameSingleBox.gamePageState.setState(() {}); // Değişiklikler uygulanıyor.
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state)
  {
    if(isMenuActive) return;
    setState(() {
      if (expactingNum != properties.num) return;
      properties.color = Colors.transparent;
      properties.numVisible = false;
      if (properties.num == totalGridSize) {
        endGame();
        return;
      }
      expactingNum++;
      highligthNextBox();
    });
  }

  @override
  startGame()
  {
    super.startGame();
    highligthNextBox();
  }
}
