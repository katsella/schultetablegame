import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';

// ignore: must_be_immutable
class MemoryGamePage extends GamePage {
  int gridSize;
  MemoryGamePage(this.gridSize);
  GamePageState gamePageState = MemoryPageState(5);
  @override
  GamePageState createState() {
    return gamePageState = MemoryPageState(this.gridSize);
  }
}

class MemoryPageState extends GamePageState {
  bool isTimePassed = false;

  MemoryPageState(int gridSize)
      : super("Hafıza", "memory", Colors.brown, gridSize) {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state) {
    if (isMenuActive) return;
    setState(() {
      if (!isTimePassed) return;
      if (expactingNum != properties.num) {
        if (properties.num > expactingNum) state.showNumTemporary();
        return;
      }
      properties.color = Colors.transparent;
      if (properties.num == totalGridSize) {
        endGame();
        return;
      }
      expactingNum++;
    });
  }

  @override
  startGame() {
    isTimePassed = false;
    super.startGame();
    showBoxes();
    Future.delayed(
      Duration(seconds: 3),
      () => {hideBoxes(), isTimePassed = true},
    );
  }
}
