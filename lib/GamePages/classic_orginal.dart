import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';

// ignore: must_be_immutable
class ClassicOrginalGamePage extends GamePage {
  int gridSize;
  ClassicOrginalGamePage(this.gridSize);
  GamePageState gamePageState = ClassicOrginalPageState(5);
  @override
  GamePageState createState() {
    return gamePageState = ClassicOrginalPageState(this.gridSize);
  }
}

class ClassicOrginalPageState extends GamePageState {
  Color _oldColor;
  ClassicOrginalPageState(int gridSize)
      : super("Klasik Orjinal", "classic_orginal", Colors.purple, gridSize) {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
    _oldColor = boxColor;
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state) {
    if(isMenuActive) return;
    setState(() {
      if (expactingNum != properties.num) return;
      properties.color = Colors.red;
      Future.delayed(
        Duration(milliseconds: 100),
        () => state.setState(() {
          properties.color = _oldColor;
        }),
      );
      if (properties.num == totalGridSize) {
        endGame();
        return;
      }
      expactingNum++;
    });
  }

  @override
  startGame() {
    super.startGame();
    showBoxes();
  }
}
