import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';

// ignore: must_be_immutable
class ClassicOrginalhReverseGamePage extends GamePage {
  int gridSize;
  ClassicOrginalhReverseGamePage(this.gridSize);
  GamePageState gamePageState = ClassicOrginalReversePageState(5);
  @override
  GamePageState createState() {
    return gamePageState = ClassicOrginalReversePageState(this.gridSize);
  }
}

class ClassicOrginalReversePageState extends GamePageState {
  Color _oldColor;
  ClassicOrginalReversePageState(int gridSize)
      : super("Klasik Orjinal Ters", "classic_orginal_reverse", Colors.teal, gridSize) {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
    _oldColor = boxColor;
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state) {
    if(isMenuActive) return;
    setState(() {
      if (expactingNum != properties.num) return;
      properties.color = Colors.red;
      Future.delayed(
        Duration(milliseconds: 100),
        () => state.setState(() {
          properties.color = _oldColor;
        }),
      );
      if (properties.num == 1) {
        endGame();
        return;
      }
      expactingNum--;
    });
  }

  @override
  startGame() {
    super.startGame();
    expactingNum = totalGridSize;
    showBoxes();
  }

  @override
  resetGame() {
    super.resetGame();
    this.expactingNum = totalGridSize;
  }
}
