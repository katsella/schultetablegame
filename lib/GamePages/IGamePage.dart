import 'dart:math';

import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';
import 'package:schultetablegame/statsBar.dart';

// ignore: must_be_immutable
abstract class GamePage extends StatefulWidget {
  int gridSize = 5;
  GamePageState gamePageState;
}

abstract class GamePageState extends State<GamePage>
    with GSBControls {
  final Color menuColor;
  final String name, tag;
  Color boxColor = Colors.white;
  int gridSize = 5;
  int totalGridSize;
  List<GameSingleBox> gridItems = [];
  bool gridItemsCreated = false;
  StatsBar statsBar;
  int expactingNum = 1;
  bool boxNumVisible = true;
  Alignment _menuAlignment;
  double _menuOpacity = 0.0;
  bool isMenuActive = true;

  GamePageState(this.name, this.tag, this.menuColor, this.gridSize) {
    statsBar = StatsBar(name, gridSize.toString());
    _menuAlignment = Alignment.topCenter.add(Alignment.topCenter);
  }

  @override
  void setState(function) {
    // setState, dispose'dan sonra çağırılırsa hatayı engelliyor.(Future işlemlerinde oluyor bu hata)
    if (mounted) {
      super.setState(function);
    }
  }

  shuffleNums(List<GameSingleBox> list) {
    int temp = 0;
    int randomIndex=0;
    for (int i = 0; i < list.length; i++) {
      randomIndex = Random().nextInt(9999999)%list.length;
      temp = list[i].gamePageState.gsbProperties.num;
      list[i].gamePageState.gsbProperties.num = list[randomIndex].gamePageState.gsbProperties.num;
      list[randomIndex].gamePageState.gsbProperties.num = temp;
      
      list[i].gamePageState.setState(() {
          list[i].gamePageState.gsbProperties.color = boxColor;
          list[i].gamePageState.gsbProperties.numVisible = boxNumVisible;
        });
    }
  }

  showMenu() {
    isMenuActive = true;
    setState(() {
      _menuAlignment = Alignment.center;
      _menuOpacity = 0.8;
    });
  }

  hideMenu() {
    isMenuActive = false;
    setState(() {
      _menuAlignment = Alignment.topCenter.add(Alignment.topCenter);
      _menuOpacity = 0.0;
    });
  }

  createGrid() {
    if (gridItemsCreated) return;
    totalGridSize = gridSize * gridSize;
    for (int i = 1; i <= totalGridSize; i++) {
      gridItems
          .add(GameSingleBox(GSBProperties(i, boxColor, boxNumVisible), this));
    }
    gridItems.shuffle();
    gridItemsCreated = true;
  }

  hideBoxes() {
    if (gridItems.length <= 0) return;
    setState(() {
      gridItems.forEach((element) {
        element.gamePageState.gsbProperties.numVisible = false;
        element.gamePageState.setState(() {});
      });
    });
  }

  showBoxes() {
    if (gridItems.length <= 0) return;
    setState(() {
      gridItems.forEach((element) {
        element.gamePageState.gsbProperties.numVisible = true;
        element.gamePageState.setState(() {});
      });
    });
  }

  @mustCallSuper
  startGame() {
    setState(() {
      statsBar.statsBar.timerStart();
      hideMenu();
    });
  }

  @mustCallSuper
  resetGame() {
    expactingNum = 1;
    this.setState(() {
      shuffleNums(gridItems);
    });
  }

  selectedItem(GSBProperties gsbProperties, GameSingleBoxState state);

  @mustCallSuper
  endGame() {
    statsBar.statsBar.timerStop();
    Future.delayed(
      // En son kutunun tıklama animasyonu gözüksün diye biraz süre tanıdım
      Duration(milliseconds: 100),
      () => {resetGame(), showMenu()},
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => {showMenu()});
  }

  @override
  Widget build(BuildContext context) {
    createGrid();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text(
          name,
          textAlign: TextAlign.center,
        ),
      ),
      backgroundColor: Colors.white60,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomLeft,
            colors: [Color(0xffE3E3E3), Color(0xffBFBFBF)],
          ),
        ),
        child: Stack(
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.height - 156,
                    maxHeight: MediaQuery.of(context).size.width + 20),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                  color: Colors.transparent,
                ),
                child: Column(
                  children: [
                    statsBar,
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                        ),
                        child: GridView.count(
                          crossAxisCount: gridSize,
                          childAspectRatio: 1,
                          children: gridItems
                              .map(
                                (data) => data,
                              )
                              .toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () => {startGame()},
              child: Align(
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 1200),
                  curve: Curves.elasticOut,
                  alignment: _menuAlignment,
                  child: Opacity(
                    opacity: _menuOpacity,
                    child: Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                      color: menuColor,
                      child: new Text(
                        name + '\nBaşlamak İçin Dokun',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
