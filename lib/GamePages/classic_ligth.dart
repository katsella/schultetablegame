import 'package:flutter/material.dart';
import 'package:schultetablegame/GamePages/IGamePage.dart';
import 'package:schultetablegame/GamePages/GameSingleBox.dart';

// ignore: must_be_immutable
class ClassicLigthGamePage extends GamePage {
  int gridSize;
  ClassicLigthGamePage(this.gridSize);
  GamePageState gamePageState = ClassicLigthPageState(5);
  @override
  GamePageState createState() {
    return gamePageState = ClassicLigthPageState(gridSize);
  }
}

class ClassicLigthPageState extends GamePageState {
  ClassicLigthPageState(int gridSize)
      : super("Klasik Işık", "classic_ligth", Colors.red, gridSize) {
    boxNumVisible = false;
    statsBar.statsBar.backColor = menuColor;
  }

  @override
  selectedItem(GSBProperties properties, GameSingleBoxState state) {
    if(isMenuActive) return;
    setState(() {
      if (expactingNum != properties.num) return;
        properties.color = Colors.transparent;
        properties.numVisible = false;
      if (properties.num == totalGridSize) {
        endGame();
        return;
      }
      expactingNum++;
    });
  }

  @override
  startGame() {
    super.startGame();
    showBoxes();
  }
}
