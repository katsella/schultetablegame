import 'package:shared_preferences/shared_preferences.dart';

class DataOperation {
  Future<int> getIntData(String dataName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(dataName) ?? 0;
  }

  Future<bool> setIntData(String dataName, int data) async {
     SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt(dataName, data);
  }

  Future<String> getStringData(String dataName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(dataName) ?? "";
  }

  Future<bool> setStringData(String dataName, String data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(dataName, data);
  }

  Future<double> getDoubleData(String dataName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getDouble(dataName) ?? 0.0;
  }

  Future<bool> setDoubleData(String dataName, double data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setDouble(dataName, data);
  }
}